var mongoose = require("mongoose");

var Schema=  mongoose.Schema;

var paymentSchema = new Schema({

  orderId:Number,
  order_id:Schema.Types.ObjectId,
  companyName:String,
  company_id:Schema.Types.ObjectId,
  totalPrice:Number,
  status:{type:String,default:"pending"}

})

module.exports = mongoose.model("paymentSchema",paymentSchema);
