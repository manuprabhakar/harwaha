app.controller("workshopController",function($scope,$http,$mdDialog,localStorageService,apiService,$state,$filter){

 $scope.assignArray = ["assign1","assign2","assign3","assign4"];

 $scope.assign = {name:null,id:null} ;

 (function() {
   var callback = apiService.getAllOrders();
   callback.then(function(data){
     $scope.orderArray = data.data;
     console.log($scope.orderArray);
     for(i in $scope.orderArray){
       $scope.orderArray[i].createdAt = $filter("date")($scope.orderArray[i].createdAt,'dd/MM/yyyy');
       $scope.orderArray[i].dateModified = $filter("date")($scope.orderArray[i].dateModified,'dd/MM/yyyy');
       $scope.orderArray[i].paymentRecievedAt = $filter("date")($scope.orderArray[i].paymentRecievedAt,'dd/MM/yyyy');
      //  $scope.orderArray[i].totalPrice=$filter('currency')($scope.orderArray[i].totalPrice,'₹', 2)
     }
   });
 }
)();


$scope.assignUpdate = function(id,assignment) {

$scope.assign.name=assignment.assignment.$viewValue;

$scope.assign.id = id;

var callback  = apiService.assignUpdate($scope.assign);

 callback.then(function(data) {
   if(data.data.success)
    {
      alert("Assignment Successfull");
      $state.reload()
    }
    else{
      alert("oops something went wrong");
    }

 });

 };



});
