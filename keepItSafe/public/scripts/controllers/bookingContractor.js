app.controller("bookingContractor",function($scope,apiService,$http,$mdToast,$state,$mdSidenav,$filter,localStorageService,$q,$stateParams){

$scope.user=localStorageService.cookie.get('name');
var login=localStorageService.cookie.get("login");
(function() {
  var openBookingCallback = apiService.showAllBookings();
  openBookingCallback.then(function(data) {
    $scope.order = data.data;
    for(i in $scope.order){
          $scope.order[i].createdAt = $filter("date")($scope.order[i].createdAt,'dd/MM/yyyy');
        $scope.order[i].deliveryDate = $filter("date")($scope.order[i].deliveryDate,'dd/MM/yyyy');}
  });
})();

(function() {
  var myBookingCallBack = apiService.getBookingsById(login.id);
      myBookingCallBack.then(function(data) {
    $scope.myorder = data.data;
    for(i in $scope.myorder){
          $scope.myorder[i].createdAt = $filter("date")($scope.myorder[i].createdAt,'dd/MM/yyyy');
        $scope.myorder[i].deliveryDate = $filter("date")($scope.myorder[i].deliveryDate,'dd/MM/yyyy');}
  });
})();
$scope.toggleLeft = buildToggler('left');
   $scope.toggleRight = buildToggler('right');

   function buildToggler(componentId) {
     return function() {
       $mdSidenav(componentId).toggle();
     };
   }


$scope.confirm=function(item){
  $scope.update={id:item.rowId,cname:login.username,cId:login.id};
  var updateordercallback=apiService.updateOrder($scope.update);
  updateordercallback.then(function(data){
    console.log(data);
    if(data.data.success){
      alert("Order Confirmed Successfully. You can see the orders in My Orders section");
      $state.reload();
    }
    else{
      alert("Something Went wrong");
      $state.reload();
    }
  });
}


   $scope.logout = function() {
     localStorageService.cookie.clearAll();
     $http.defaults.headers.common.Authorization = '';
     $state.go("createOrder");
   };
});
