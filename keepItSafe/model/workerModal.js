var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var workerSchema = new Schema({
 name:String,
 address:String,
 phone:Number,
 smartphone:String,
 vehicle:String,
 outstation:String,
 qualification:String,
 workarea:[String],
 locality:[String],
 isbooked:Number,
 aadharimg:String
});

module.export = mongoose.model('workerSchema',workerSchema);
