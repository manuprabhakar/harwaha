var mongoose = require("mongoose");

var Schema = mongoose.Schema;
autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection("mongodb://test:test@localhost:27017/tododb");

autoIncrement.initialize(connection);

var productQuantitySchema = new Schema({

   name:{type:String},
	 quantity:{type:Number},
   rate:{type:Number}


});

var orderSchema = new Schema({
   orderId:{type:Number, index:{unique: true }},
   userid:String,
   username:String,
   phone:String,
   email:String,
	 totalPrice:Number,
   numberofworkers:String,
   worktype:String,
   locality:String,
   createdAt:{type:Date ,default:Date.now},
   deliveryDate:{type:Date ,default:Date.now},
   orderType: {type:String ,default:'notSelected'},
   paymentStatus:{type:String,default:'notRecieved'},
   paymentRecievedAt:{type:Date,default:Date.now},
   contractor:{type:String,default:'notAssigned'},
   contractorName:{type:String,default:'notAssigned'}
});

var order = connection.model('order', orderSchema);

orderSchema.plugin(autoIncrement.plugin, {
    model: 'order',
    field: 'orderId',
    startAt: 1000,
    incrementBy: 1
});

module.export = mongoose.model('orderSchema',orderSchema);
