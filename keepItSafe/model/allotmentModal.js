var mongoose = require("mongoose");

var Schema = mongoose.Schema;
var moment = require('moment');
var current=moment().format('d/MMM/YYYY');
var productAllotmentSchema = new Schema({
     product:{type:String},
	 supplier:{type:String},
	 container:{type:String},
	 rate:{type:Number},
	 quantityDispatched:{type:Number},
	 mark:{type:String},
	 vendor:{type:String},
	 status:{ type: String, default: 'NR'},
	 recieved:{ type: Date, default: '01/13/1992' },
	 created:{ type: Date, default: Date.now }
});

var allotmentSchema = new Schema({
 product:String,
 requestedQuantity:Number,
 allotment:[productAllotmentSchema]
});

module.export = mongoose.model('allotmentSchema',allotmentSchema);
