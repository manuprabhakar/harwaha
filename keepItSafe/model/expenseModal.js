var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var expenseSchema = new Schema({

   type:{type:String},
   paidFor:{type:String},
   rate:{type:Number},
   createdAt:{type:Date,default:Date.now}

});

module.export = mongoose.model('expenseSchema',expenseSchema);
