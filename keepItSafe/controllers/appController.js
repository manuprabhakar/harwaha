var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
Login = mongoose.model("loginSchema");
Worker = mongoose.model("workerSchema");
Order = mongoose.model("orderSchema");
Product=mongoose.model("productSchema");
Company =mongoose.model("companySchema");
Allotment =mongoose.model("allotmentSchema");
AllotmentOption = mongoose.model("allotmentOptionSchema");
Payment = mongoose.model("paymentSchema");
Expense = mongoose.model("expenseSchema");
json2xls = require("json2xls");
var express=require('express');
var request = require('request');

var jwt = require("jsonwebtoken");
var moment = require("moment");
var fs = require("fs");
var http = require('http');
var moment = require('moment');
var AlotArray = [];
var finalAlot = [];
var MongoClient = require('mongodb').MongoClient;
var config = require('../config');
var multer = require('multer');
var otp=express.Router();
var otpverify=express.Router();

exports.otp=function(req,res){
var options = {
"method": "GET",
"hostname": "2factor.in",
"port": null,
"path": "/API/V1/822ba5d1-946c-11e6-96db-00163ef91450/SMS/"+req.body.number+"/AUTOGEN",
"headers": {}
};
var req = http.request(options, function (res) {
var chunks = [];
res.on("data", function (chunk) {
chunks.push(chunk);
});
res.on("end", function () {
var body = Buffer.concat(chunks);
var obj = JSON.parse(body);
if(obj.Status=='Error') {ef(obj);}else{suc(obj);}
});
});
req.write("{}");
req.end();
var ef=function(obj){console.log(obj);res.json({ success: false, message: obj.Details });}
var suc=function(obj){res.json({ success: true, message: obj.Details });}
}

exports.otpverify=function(req,res){
var options = {
"method": "GET",
"hostname": "2factor.in",
"port": null,
"path": "/API/V1/822ba5d1-946c-11e6-96db-00163ef91450/SMS/VERIFY/"+req.body.sessionId+"/"+req.body.pass,
"headers": {}
};
var req = http.request(options, function (res) {
var chunks = [];
res.on("data", function (chunk) {
chunks.push(chunk);
});
res.on("end", function () {
var body = Buffer.concat(chunks);
var obj = JSON.parse(body);
if(obj.Status=='Error') {ef(obj);}else{suc(obj);}
});
});
req.write("{}");
req.end();
var ef=function(obj){console.log(obj);res.json({ success: false, message: obj.Details });}
var suc=function(obj){res.json({ success: true, message: obj.Details });}
}



exports.createUser = function(req,res) {
	var newUser = new Login(req.body);
	newUser.save(function(err,login) {
		if(err)
		res.send(err);
		res.json({ success: true, message: 'Account created successfully' });
	});
};

exports.createWorker=function(req,res) {
  var newWorker=new Worker(req.body);
  newWorker.save(function(err){
    if(err)
    res.send(err);
    res.json({success: true, message:'Worker created successfully'});
  });
};
exports.findWorker = function(req,res){
	var data=req.body.workarea;
	var ct=req.body.worker_no;
	var _count=false;
  Worker.find({workarea:data,isbooked:1},function(err,worker) {

  if (err)
  res.send(err);
	if(worker.length>=ct)
	_count=true;


	res.json({"isCount":_count});

  });
};

	exports.updateWorker = function(req,res){

     Worker.update({_id:req.body._id},{name:req.body.name,address:req.body.address,phone:req.body.phone,
		 smartphone:req.body.smartphone,vehicle:req.body.vehicle,qualification:req.body.qualification,
	 outstation:req.body.outstation,workarea:req.body.workarea,locality:req.body.locality,isbooked:req.body.isbooked,
 aadharimg:req.body.aadharimg},{upsert:true,safe:true},function(err,worker) {
			 if(err)
			 res.send(err);
	 		res.json({ success: true, message: 'Worker updated successfully' });

     });


	};

exports.showAllWorker = function(req,res) {
       Worker.find({},function(err,worker){
            if(err)
            res.send(err)
            res.send(worker);
       });
};
exports.upload= function(req,res){
    upload(req,res,function(err){
      if(err){
           res.json({error_code:1,err_desc:err});
           return;
      }

       res.json({error_code:0,err_desc:null});

});
};
exports.showAllBookings = function(req,res) {
       Order.find({contractor:'notAssigned'},function(err,order){
            if(err)
            res.send(err)
            res.send(order);
       });
};
exports.adminShowBookings = function(req,res) {
       Order.find({},function(err,order){
            if(err)
            res.send(err)
            res.send(order);
       });
};
exports.adminShowContractor = function(req,res) {
       Login.find({role:'contractor'},function(err,contractor){
            if(err)
            res.send(err)
            res.send(contractor);
       });
};

var storage = multer.diskStorage({ //multers disk storage settings
			destination: function (req, file, cb) {
					cb(null, './uploads/')
			},
			filename: function (req, file, cb) {

				cb(null, file.originalname)

			}
	});
	var upload = multer({ //multer settings
									storage: storage
							}).single('file');


exports.updateOrder = function(req,res) {
Order.update({_id:req.body.id},{contractor:req.body.cId,contractorName:req.body.cname},{upsert:false},function(err,order) {

								if(err)
								res.send(err);
								res.json({ success: true, message: 'Order Status updates  successfully' });

							});
							};

exports.getBookingsById = function(req,res) {
	Order.find({contractor:req.body},function(err,order) {
		if(err)
		res.send(err);
		res.send(order);
	});
};


// product API

exports.createProduct = function(req,res) {
	var newProduct = new Product(req.body);
	newProduct.save(function(err,product) {
		if(err)
		res.send(err);
		res.json({ success: true, message: 'Product created successfully' });
	});
};

exports.findProductQuery = function(req,res){

    var data = req.body.name;
	  Product.find({name: new RegExp(data, 'i')},function(err,product) {
			if(err)
			res.send(err);
			res.send(product);
	  });
};



exports.showAllProducts = function(req,res) {
       Product.find({},function(err,product){
            if(err)
            res.send(err)
            res.send(product);

       });

};


exports.deleteProduct = function(req,res){
  Product.remove({_id:{$in:req.body}},function(err,product) {
  	if(err)
		res.send(err);
		res.json({ success: true, message: 'product deleted successfully' });
  });
};


	exports.updateProduct = function(req,res){

     Product.update({_id:req.body._id},{name:req.body.name,rate:req.body.rate},{upsert:false,safe:true},function(err,product) {
			 if(err)
			 res.send(err);
	 		res.json({ success: true, message: 'product updated successfully' });

     });


	};

//***********************************************company API start******************************************************


exports.createCompany = function(req,res) {
	var newCompany = new Company(req.body);
	newCompany.save(function(err,Company) {
		if(err)
		res.send(err);
		res.json({ success: true, message: 'Product created successfully' });
	});
};

exports.showAllCompany = function(req,res) {
       Company.find({},function(err,company){

            if(err)
            res.send(err)
            res.send(company);

       });
};


exports.deleteWorker = function(req,res){
  Worker.remove({_id:{$in:req.body}},function(err,company) {
  	if(err)
		res.send(err);
		res.json({ success: true, message: 'Worker Data Deleted Successfully' });
  });
};



	exports.updateCompany = function(req,res){

     Company.update({_id:req.body._id},{name:req.body.name,address:req.body.address,phone:req.body.phone},{upsert:true,safe:true},function(err,company) {
			 if(err)
			 res.send(err);
	 		res.json({ success: true, message: 'product updated successfully' });

     });


	};

exports.getCompanyById = function(req,res){
	Company.find({_id:req.body.id},function(err,company){
		if (err) {
			res.send(err);
		}
		res.send(company);
	});
};

	exports.findCompanyQuery = function(req,res){

	    var data = req.body.name;
		  Company.find({name: new RegExp(data, 'i'),route:req.body.route},function(err,product) {

				if(err)
				res.send(err);
				res.send(product);

		  });

	};


	exports.updateBalance = function(req,res) {

		Company.update({_id:req.body._id},{ $inc: { balance:req.body.balance} },{upsert:false,safe:true},function (err,company){
			if(err)
			res.send(err);
		 res.json({ success: true, message: 'Balance updated successfully' });
	 });
 };


 exports.updateBalanceDec = function(req,res) {

 	Company.findOneAndUpdate({_id:req.body._id},{ $inc: { balance:-req.body.balance} },{upsert:false,safe:true},function (err,company){

		 if(req.body.balance>company.balance){
			 Company.update({_id:company._id},{balance:0},function(err,company1) {
        if (err) {
        	console.log(err);
        }

			 });

		 }


			if(err)
			 res.send(err);
			res.json({ success: true, message: 'Balance updated successfully' });


  });
 };

// exports.getCompanyByName = function(req,res) {
// Company.find({name:req.body.companyName},function(err,company) {
// 	if (err) {
// 		res.send(err);
// 	}
// 	else {
// 		res.send(company)
// 	}
//
// });
// };




//end

//**********************************************Payment API start************************************************************

exports.createPayment = function(req,res) {

var newPayment = new Payment(req.body);
newPayment.save(function(err,payment) {
	if(err)
	res.send(err);
	res.json({ success: true, message: 'Payment updated  successfully' });
	res.send(payment);
});

};

exports.getAllPayment = function(req,res){
 Payment.find({},function(err,payment) {
	 if(err)
 	res.send(err);
	res.send(payment);

 });

};

exports.updatePayment = function(req,res) {
Payment.update({_id:req.body._id},{status:req.body.status},{upsert:false},function(err,payment) {

	if(err)
	res.send(err);
	res.json({ success: true, message: 'Payment updated  successfully' });

});

};





//**********************************************payment API end**************************************************************

//**********************************************Allotment API start************************************************************

exports.createAllotmentProduct = function(req,res) {
 var newAllotment = new Allotment(req.body);
	newAllotment.save(function(err,allotment){
		 if(err)
		 res.send(err);
		 res.json({ success: true, message: 'Allotment product created successfully' });
	});
};

exports.showAllotments = function(req,res) {
	Allotment.find({},function(err,allotment){

			  if (err)
			  res.send(err)
			  res.send(allotment)

	});
};

exports.updateAllotmentProduct = function(req,res) {
Allotment.update({_id:req.body._id},
		{
		$push:{
		allotment:
		{
			supplier:req.body.supplier,
			container:req.body.container,
			rate:req.body.rate,
			quantityDispatched:req.body.quantityDispatched,
			mark:req.body.mark,
			vendor:req.body.vendor,
			status:req.body.status,
			product:req.body.product
		}

}

},{upsert:true},function(err,allotment) {
		if(err)
		res.send(err)
		res.json({ success: true, message: 'Allotment created successfully' });
	});
};

exports.createAllotmentOption = function(req,res){
	var newAllotmentOption = new AllotmentOption(req.body);
	newAllotmentOption.save(function(err,AllotmentOption) {

		if(err)
			res.send(err);
			res.json({ success: true, message: 'Input done successfully' });
	});
};

exports.getAllotmentOption = function(req,res) {
  AllotmentOption.find({},function(err,allotment) {

		 if(err)
		 res.send(err);
		 res.send(allotment);

  });
};



exports.updateAllotmentById = function(req,res) {
	Allotment.update({_id:req.body.id,'allotment._id':req.body.idA},{$set:{'allotment.$.status':req.body.status,'allotment.$.recieved':new Date()}},{upsert:false},function(err,product){
		if(err)
		res.send(err);
	 res.json({ success: true, message: 'Allotment updated successfully' });

	});
};



exports.getAllotmentBySupplier = function(req,res){
	Allotment.aggregate([
	{
	      $project: {
	         allotment: {
	            $filter: {
	               input: "$allotment",
	               as: "allotment",
	               cond: { $and: [
								           { $gte: [ "$$allotment.created", new Date(req.body.startAt)]},
										   { $lte: [ "$$allotment.created", new Date(req.body.endAt)]},
											{  $eq:["$$allotment.supplier",req.body.supplier] }
			                   ],


	            }
	         }
	      }
	   }
	 }],function(err,allotment){
 		if(err)
 		{res.send(err);}
		else {
			if(allotment[0].allotment.length === 0)
			{
				res.json({success: false, message: 'no data found'})
			}

			else
			{       AlotArray = allotment;
							 for(i in AlotArray )
							 {
								for(j in AlotArray[i].allotment)
								AlotArray[i].allotment[j].created = moment(AlotArray[i].allotment[j].created).format('MM/DD/YYYY');
								AlotArray[i].allotment[j].recieved = moment(AlotArray[i].allotment[j].recieved).format('MM/DD/YYYY');
							 	finalAlot.push(AlotArray[i].allotment[j]);

							 }

							 // console.log();
						 	 // res.send(finalAlot);
						 	 var xls = json2xls(finalAlot);
						     fs.writeFileSync('data.xlsx', xls, 'binary');
						     console.log("file saved");
						     res.download('data.xlsx',function(err){
						     	if(err){
						     		console.log(err);
						     	}
						     });
			}


            }

 });

};

var totalQuantity = {
	                    status:"Total Product in Transit",
						quantityDispatched:0
					}



exports.getAllotmentByStatus = function(req,res){
	Allotment.aggregate([
	{
	      $project: {
	         allotment: {
	            $filter: {
	               input: "$allotment",
	               as: "allotment",
	               cond: { $and: [

									 {  $eq:["$$allotment.status",req.body.status] }
			                   ],


	            }
	         }
	      }
	   }
	 }],function(err,allotment){
 		if(err)
 		{res.send(err);}
		else {

				 AlotArray = allotment;
				 for(i in AlotArray )
				 {
					for(j in AlotArray[i].allotment)
					AlotArray[i].allotment[j].created = moment(AlotArray[i].allotment[j].created).format('MM/DD/YYYY');
					AlotArray[i].allotment[j].recieved = moment(AlotArray[i].allotment[j].recieved).format('MM/DD/YYYY');
					totalQuantity.quantityDispatched = totalQuantity.quantityDispatched+AlotArray[i].allotment[j].quantityDispatched;
				 	finalAlot.push(AlotArray[i].allotment[j]);
				 }
				 finalAlot.push(totalQuantity);

				 // console.log();
			 	//  res.send(finalAlot);
			 	   var xls = json2xls(finalAlot);
			     fs.writeFileSync('data.xlsx', xls, 'binary');
			     console.log("file saved");
			     res.download('data.xlsx');


            }

 });

};


exports.killTheDb = function (req,res) {

	db = mongoose.createConnection(config.database);

  db.dropDatabase(function(err) {
		if(err)
		res(err);
		res.json({ success: true, message: 'database deleted successfully' });

  });


};




//**********************************************Allotment API end************************************************************

//**********************************************Expense API Start************************************************************

exports.createExpense = function(req,res) {
var expense = new Expense(req.body);
expense.save(function(err,expense){
	if(err)
	res.send(err);
	res.json({ success: true, message: 'Expense created successfully' });
});
};

exports.getExpenses = function(req,res) {
  Expense.find({},function(err,expense) {

		 if(err)
		 res.send(err);
		 res.send(expense);

  });
};


exports.getExpensesByType = function(req,res) {
  Expense.find(
		           {
								 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)},
			           "type":{$eq:req.body.type}
					     },{_id:0,__v:0},{lean:true},function (err,expense) {

						   if(err)
							 {
								 res.send(err);

							 }

							 else
							 {



												for(i in expense){
													expense[i].created = moment(expense[i].created).format('MM/DD/YYYY');
												}



								      var xls = json2xls(expense);
								      fs.writeFileSync('data2.xlsx', xls, 'binary');
								      console.log("file saved");
								      res.download("data2.xlsx");

							 }

					});

			};




//**********************************************Expense API end************************************************************

//**********************************************Order API start************************************************************


exports.createOrder = function(req,res){
	 var newOrder = new Order(req.body);
	 newOrder.save(function(err,orderSchema) {
		 if(err)
		 res.send(err);
		 res.json({ success: true, message: 'order created successfully' });
	 });
};

exports.getAllOrders = function(req,res) {
	Order.find({},null,{sort:{"_id":-1}},function(err,order) {
    if(err)
		res.send(err);
		res.send(order);

	});
};

exports.getUserOrder = function(req,res) {

	Order.find({user:req.body.user},null,{sort:{"_id":-1}},function(err,order) {

		if(err)
	 res.send(err);
	 res.send(order);

	});

};

exports.updateOrderTele = function(req,res) {
Order.update({_id:req.body.id},{status:req.body.status,dateModified:req.body.dateModified},{upsert:false},function(err,order) {

 if(err)
 res.send(err);
 res.json({ success: true, message: 'Order Status updates  successfully' });

});
};

exports.getOrderByQuery = function(req,res) {


if (req.body.startAt==null&&req.body.endAt==null) {

	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus}},function(err,order){

						 if(err)
						 res.send(err);
						 res.send(order);

					 });

}

else if(req.body.startAt==null){

	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus},


						"createdAt": {$lte:new Date(req.body.endAt)}

					},function(err,order){

						if(err)
						res.send(err);
						res.send(order);

					});


}

else if(req.body.endAt==null){

	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus},


						"createdAt": {$gte:new Date(req.body.startAt)}

					},function(err,order){

						if(err)
						res.send(err);
						res.send(order);

					});


}


else{


	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus},


						 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)}

					 },function(err,order){

						 if(err)
						 res.send(err);
						 res.send(order);

					 });
}


};

exports.getOrderByCompanyId = function(req,res){
Order.find({company_id:req.body.companyId},null,{sort: {'_id': -1}},function(err,order) {
	if(err)
	res.send(err);
  res.send(order);
});

};

exports.getOrderById = function(req,res) {
Order.find({userid:req.body.id},function(err,order) {
	   if(err)
		 res.send(err);
		 res.send(order);
});


};

exports.updateOrderStatus = function(req,res) {
Order.update({_id:req.body.id},{status:req.body.status},{upsert:false},function(err,order) {

	if(err)
	res.send(err);
	res.json({ success: true, message: 'Order Status updates  successfully' });

});

};



exports.updateOrderPaymentStatus = function(req,res) {
Order.update({_id:req.body.id},{paymentStatus:req.body.status},{upsert:false},function(err,order) {

	if(err)
	res.send(err);
	res.json({ success: true, message: 'Order Payment Status updated  successfully' });

});

};

exports.getOrderByUser = function(req,res) {
  Expense.find(
		           {
								 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)},
			           "user":{$eq:req.body.user}
					     },{_id:0,__v:0},{lean:true},function (err,expense) {

						   if(err)
							 {
								 res.send(err);

							 }

							 else
							 {



												for(i in expense){
													expense[i].created = moment(expense[i].created).format('MM/DD/YYYY');
												}



								      var xls = json2xls(expense);
								      fs.writeFileSync('data2.xlsx', xls, 'binary');
								      console.log("file saved");
								      res.download("data2.xlsx");

							 }

					});

			};



exports.assignUpdate = function(req,res) {
Order.update({_id:req.body.id},{assignedTo:req.body.name,status:req.body.status},{upsert:false},function(err,order) {

	if(err)
 res.send(err);
 res.json({ success: true, message: 'Order Payment Status updated  successfully' });


});

};


exports.getOrderReport = function(req,res) {
  Order.find(
		           {
								 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)},
			           "user":{$eq:req.body.user}
					     },{_id:0,__v:0},{lean:true},function (err,order) {

						   if(err)
							 {
								 res.send(err);

							 }

							 else
							 {



												for(i in order){
													order[i].createdAt = moment(order[i].created).format('MM/DD/YYYY');
												}



								      var xls = json2xls(order);
								      fs.writeFileSync('data2.xlsx', xls, 'binary');
								      console.log("file saved");
								      res.download("data2.xlsx");

							 }

					});

			};






//**********************************************Order API end************************************************************


exports.authenticate = function(req,res){
  Login.findOne({
		email:req.body.email
	},function(err,user){

		if(err)
		res.send(err);

		if(!user){
			res.json({success:false,message:"Authentication Failed! user not found!"});

		} else if (user) {

			if(user.password != req.body.password)
			{
				res.json({success:false,message:"Authentication Failed! Wrong Password!"});
			} else{

        var token = jwt.sign(user,app.get('superSecret'),{
				 	expiresIn:'1d'
				 });

				res.json({
          success: true,
          message: 'Enjoy your token!',
          role:user.role,
					user:user.user,
					token: token,
					username:user.name,
					phone:user.phone,
					email:user.email,
					id1:user._id
        });

			}
		}


	});






};









//
// exports.showUser = function(req,res) {
// 	Login.find({},function(err,user){
// 	if(err)
// 	res.send(err);
// 	res.send(user);
// 	});
// };
