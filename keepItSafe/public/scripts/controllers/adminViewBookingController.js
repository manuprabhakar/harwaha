app.controller("adminViewBookingController",function($scope,apiService,$http,$mdToast,$state,$mdSidenav,$filter,localStorageService,$q,$stateParams){
  (function() {
    var adminBookingCallback = apiService.adminBookings();
    adminBookingCallback.then(function(data) {
      $scope.adminViewBookings = data.data;
      for(i in $scope.adminViewBookings){
            $scope.adminViewBookings[i].createdAt = $filter("date")($scope.adminViewBookings[i].createdAt,'dd/MM/yyyy');
          $scope.adminViewBookings[i].deliveryDate = $filter("date")($scope.adminViewBookings[i].deliveryDate,'dd/MM/yyyy');}
    });
  })();

  (function() {
    var adminContractorCallback = apiService.adminContractor();
    adminContractorCallback.then(function(data) {
      $scope.adminViewContractor = data.data;
      // for(i in $scope.adminViewBookings){
      //       $scope.adminViewBookings[i].createdAt = $filter("date")($scope.adminViewBookings[i].createdAt,'dd/MM/yyyy');
      //     $scope.adminViewBookings[i].deliveryDate = $filter("date")($scope.adminViewBookings[i].deliveryDate,'dd/MM/yyyy');}
    });
  })();

  $scope.toggleLeft = buildToggler('left');
     $scope.toggleRight = buildToggler('right');

     function buildToggler(componentId) {
       return function() {
         $mdSidenav(componentId).toggle();
       };
     }

     $scope.logout = function() {
       localStorageService.cookie.clearAll();
       $http.defaults.headers.common.Authorization = '';
       $state.go("createOrder");
     };




});
