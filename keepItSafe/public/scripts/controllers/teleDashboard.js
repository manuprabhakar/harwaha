app.controller("teleDashboard",function($scope,$http,apiService,$rootScope,$q,$filter,$state,localStorageService){

  $scope.orderArray = [];

  $scope.startAt = null;
  $scope.endAt = null;


  $scope.orderFilter = {
    status:'(.*)',
    paymentStatus:'(.*)',
    startAt:null,
    endAt:null
  };




    (function() {
      var callback = apiService.getUserOrder();
      callback.then(function(data){
        $scope.orderArray = data.data;
        console.log($scope.orderArray);
        for(i in $scope.orderArray){
          $scope.orderArray[i].createdAt = $filter("date")($scope.orderArray[i].createdAt,'dd/MM/yyyy');
          $scope.orderArray[i].dateModified = $filter("date")($scope.orderArray[i].dateModified,'dd/MM/yyyy');
          $scope.orderArray[i].paymentRecievedAt = $filter("date")($scope.orderArray[i].paymentRecievedAt,'dd/MM/yyyy');

        }
      });
    }
  )();




  $scope.selecteddateStart=function(){
   $scope.orderFilter.startAt = $scope.startAt;
  }

  $scope.selecteddateEnd=function(){
   $scope.orderFilter.endAt = $scope.endAt;
  };








  $scope.searchFilter = function() {
    var callback = apiService.getOrderByQuery($scope.orderFilter);

     callback.then(function(data) {

       $scope.orderArray = data.data;
       console.log($scope.orderArray);
       for(i in $scope.orderArray){
         $scope.orderArray[i].createdAt = $filter("date")($scope.orderArray[i].createdAt,'dd/MM/yyyy');
         $scope.orderArray[i].dateModified = $filter("date")($scope.orderArray[i].dateModified,'dd/MM/yyyy');
         $scope.orderArray[i].paymentRecievedAt = $filter("date")($scope.orderArray[i].paymentRecievedAt,'dd/MM/yyyy');
         $scope.orderArray[i].totalPrice=$filter('currency')($scope.orderArray[i].totalPrice,'₹', 2)

       }
     });

  };

  $scope.editOrder = function(item) {
    console.log(item);
  };


  $scope.checkForStatus = function(item) {
    for(i in $scope.orderArray)
      {
        if(($scope.orderArray[i]._id==item.rowId)&&($scope.orderArray[i].status=="returned"||$scope.orderArray[i].status=="cancelled"))
        return true;
      }

        return false;

  };



  //for order update views

  $scope.updateDate = new Date();





  $scope.findMe = function(item,status) {
    console.log(status);
    for(i in $scope.orderArray){
      if($scope.orderArray[i]._id==item.rowId)
        console.log($scope.orderArray[i].totalPrice);

    }
  };



   $scope.updateOrderTele = function(item,status) {

    var updateArray = {
      status:status,
      totalPrice:null,
      id:item.rowId,
      company_id:null

    };


    for(i in $scope.orderArray){
      if($scope.orderArray[i]._id==item.rowId)
      {  updateArray.totalPrice = $scope.orderArray[i].totalPrice;
          updateArray.company_id = $scope.orderArray[i].company_id;
      }

    };




     var request = [apiService.updateOrderTele(updateArray),apiService.updateBalanceDecTele(updateArray)];


      $q.all(request).then(function(data){
         if(data[0].data.success&&data[1].data.success)
         {
             // var callback = apiService.createPayment()

             alert("Order updated Successfully!!");
             $state.reload();
         }
         else {
           alert("oops something went wrong! Please try again!!")
         }

      });


   };












});
