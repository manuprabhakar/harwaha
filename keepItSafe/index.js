var express = require("express");
app = express();
port = process.env.PORT||3000;

var MongoClient = require('mongodb').MongoClient;

var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');

var mongooseBack = require("mongoose");

var json2xls = require('json2xls');

var jwt = require("jsonwebtoken");
Login =  require("./model/loginModal");
Order  = require("./model/orderModal");
Product = require("./model/productModal");
Company = require("./model/companyModal");
Alottment = require("./model/allotmentModal");
AlottmentOption = require("./model/allotmentOptionmodal");
Expense = require("./model/expenseModal");
Payment = require("./model/paymentsModal");
Worker = require("./model/workerModal")
var multer = require('multer');
var Schema = mongoose.Schema;
bodyParser = require('body-parser');
var config = require('./config');
var fs = require('fs');


mongoose.Promise = global.Promise;
mongoose.connect(config.database);
app.use(express.static(__dirname +'/public'));  // set the static files location /public/img will be /img for users
//Image Logic
app.set('superSecret',config.secret);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "http://localhost");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
  });

var routes = require('./routes/route');
routes(app);
// app.get('/', function(req, res){
//   res.redirect('/addWorker.html');
// });
var http = require('http');
//End of Image Logic



 setInterval(function(){

   db = mongoose.createConnection('mongodb://localhost:27017/copyBase');

   db.dropDatabase();

   console.log("db dropped");

 MongoClient.connect(config.database, function(err, db) {
     if(err) {
         console.log(err);
     }
     else {

         var mongoCommand = { copydb: 1, fromhost: "localhost", fromdb: "harwaha", todb: "copyBase" };
         var admin = db.admin();

         admin.command(mongoCommand, function(commandErr, data) {
             if (!commandErr) {
                 console.log(data);
             } else {
                 console.log(commandErr.errmsg);
             }
             db.close();
         });
     }
 });}, 1000*60*60*12);
app.listen(port);
console.log("app started at port 3000");
