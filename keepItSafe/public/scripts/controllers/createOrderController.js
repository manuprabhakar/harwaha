app.controller("createOrderController",function($scope,$http,apiService,$filter,$state,localStorageService,$anchorScroll,$mdDialog,$window,$location,$rootScope,formService,$modal,$log){
$scope.workarea="";
$scope.worker_no;
$scope.count;
$scope.area;
$scope.localities;
$scope.states=["Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Dadra and Nagar Haveli","Daman and Diu","Delhi","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka",
                                        "Kerala","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Puducherry","Punjab", "Rajasthan","Sikkim","Tamil Nadu",
                                        "Telangana","Tripura","Uttar Pradesh","Uttarakhand","West Bengal"];
// $scope.locality=['Alampur Gonpura','Anandpuri','Anisabad','Ashiana Nagar','Ashiana-Digha Road','Ashok Nagar','Bahadurpur','Bailey Road','Bairiya','Bakerganj','Barh','Begampur','Beldari Chak','Bhogipur','Bihta','Boring Road','Budha Colony','Chajju Bagh','Chhoti Pahari','Chitkohra','Chitragupta Nagar','Chitrakut Nagar','Danapur','Danapur Road','Danapur-Khagaul Road','Dhanaut','Digha','Dulhin Bazar','East Lakshmi Nagar','Exhibition Road','Fraser Road Area','Ganga Vihar Colony','Gardanibagh','Gaurichak','Ghrounda','Gola Road','Golambar','Gopalpur','Haji Ganj','Hajipur','Hanuman Nagar','Haroon Nagar','Indira Nagar','Indrapuri','Jagdeo Path','Jai Prakash Nagar','Jakkanpur','Jalalpur','Janipur','Kadamkuan','Kanhauli','Kankarbagh','Kankarbagh Road','Karanpura','Kautilya Nagar','Keshri Nagar','Khagaul','Khagaul Road','Khajpura','Khusropur','Kidwaipur Postal Colony','Kidwaipuri','Kothwan','Kumhrar','Kurji','Kurthoul','Muhammadpur Korji','Nageshwar Colony','Naubatpur','New Azimabad Colony','New Patliputra Colony','North Shastri Nagar','North Sri Krishna Puri','Pahari','Paijawa','Painal','Pareo','Parsa','Patel Nagar','Patliputra Colony','Patliputra Road','Patna - Bakhtiyarpur Road','Patna - Gaya Road','Phulwari Sharif','Punaichak','Punpun','Rajapur','Rajbansi Nagar','Rajeev Nagar','Rajendra Nagar','Ramjaipal Nagar','Ramkrishan Nagar','Rampur','Ranipur','Rukanpura','Rupaspur','Sabzibagh','Sadhnapuri','Sadikpur','Saguna More','Samanpura','Sandalpur','Shastri Nagar','Sheikhpura','Shikarpur','Shivala Par','Shivpuri','Sipara','Sonepur','Sri Krishna Nagar','Sri Krishna Puri','Sultangunj','Taregana','Tripolia','Vijay Nagar','Yarpur'];
$scope.selectedItem;
$scope.setRole = function () {
    $scope.workarea = $scope.selectedItem;
console.log($scope.workarea);
}
$scope.setlocality=function(){
if($scope.area=="Bihar"){
  $scope.locality=['Patna'];
}else{
$scope.locality=[''];
}
}
$scope.order={userid:$scope.userid,worktype:$scope.workarea,numberOfWorkers:$scope.worker_no,totalPrice:'',deliveryDate:$scope.date};
$scope.workerFind=function(){
  localStorageService.cookie.set('order',$scope.order);
  console.log($scope.workarea);
  if($scope.area=="Bihar"){
  var callback=apiService.findWorker($scope.workarea,$scope.worker_no);
  callback.then(function(data){
    if(data.data.isCount)
    {console.log(data);
        var worktype=$scope.workarea;
        $state.go('success',{'count':$scope.worker_no, 'area':worktype});
      }
        else {
          $state.go('failure');
        }

  });
}
  else{
    $state.go('stateNotAvailable');}
  };
$scope.user = {
  email:'',password:''
};
$scope.loginUser = function() {
  // console.log($scope.number);
       var dataCallback = apiService.loginUser($scope.user.email,$scope.user.password);

         dataCallback.then(function(data){

             $scope.data = data.data;
             if($scope.data.success)
             {console.log($scope.data);
               $scope.showAlert("Login Successfully","Hi, "+data.data.username+" have a great day!!!");
               var login = {
                 token:data.data.token,
                 user:data.data.user,
                 role:$scope.data.role,
                 username:$scope.data.username,
                 id:$scope.data.id1,
                 phone:$scope.data.phone,
                 email:$scope.data.email
               }
               console.log(login);
               localStorageService.cookie.set('login',login);
               localStorageService.cookie.set('token',login.token);
               localStorageService.cookie.set('name',login.username);
               localStorageService.cookie.set('userid',login.id);
               localStorageService.cookie.set('phone',login.phone);
               localStorageService.cookie.set('email',login.email);
               $http.defaults.headers.common.Authorization =login.token;

               if($scope.data.role=="contractor")
               {
                 $state.go('addWorker');
               }

               else if($scope.data.role=="user")
               {
                 $state.go('createOrderFinal');

               }

               else if($scope.data.role=="admin")
               {
                 $state.go('adminBookings');
               }

               else if($scope.data.role=="bookingManager")
               {
                 $state.go('bookingManager');
               }
             }
             else
             alert("wrong user");

         });
};

$scope.showAlert = function(read,message) {
    // Appending dialog to document.body to cover sidenav in docs app
    // Modal dialogs should fully cover application
    // to prevent interaction outside of dialog
    $mdDialog.show(
      $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title(read)
        .textContent(message)
        .ariaLabel('login Dialog ')
        .ok('Got it!')
    );
  };
$scope.otp={password:'',sessionId:''};
$scope.test=function(){
  console.log("RUN");

}

$scope.showOTP=false;
$scope.submit=true;
$scope.createUser={name:'',password:'',email:'',companyname:'',phone:'',location:'',state:'',role:'user'};
$scope.show=function(){
  $scope.showOTP=true;
  $scope.submit=false;
  var verifyCallback= apiService.verify($scope.createUser.phone);
  verifyCallback.then(function(data){
    console.log(data);
    if(data.data.success){
    $scope.otp.sessionId=data.data.message;}
  });
}
$scope.verify=function(){
var otpCallback=apiService.otpverify($scope.otp);
otpCallback.then(function(data){
  console.log(data);
  if(data.data.success){
    var userCallback=apiService.createUser($scope.createUser);
    userCallback.then(function(data){
      console.log(data);
      if (data.data.success){
        alert("Account Created Successfully, Login to Continue");
        $state.go('createOrder');
      }
      else{
        alert("There was some error, Please try again");
      }
    });
  }
});
}
$scope.notShow=function(){console.log("notshow")}






});
