var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var loginSchema = new Schema({
 user: String,
 password: String,
 role:String,
 name:String,
 email:String,
 companyname:String,
 phone:String,
 location:String,
 state:String,
});

module.export = mongoose.model('loginSchema',loginSchema);
