app.controller("expensesController",function($scope,$mdToast,$state,apiService,$filter){

   $scope.expenseType = ['food','recharge','convenience','water'];

   $scope.expenseArray = [];

   (function() {
 		var callback = apiService.getExpenses();
 		callback.then(function(data) {
 			$scope.expenseArray = data.data;
      for(i in $scope.expenseArray)
      {
      $scope.expenseArray[i].createdAt = $filter('date')($scope.expenseArray[i].createdAt, "d/MM/yyyy","+0530");
      }
 		});
 	})();



   $scope.expense = {
     type:null,rate:null,paidFor:null
   };

   $scope.createExpense = function() {

     var callback = apiService.createExpense($scope.expense);
       callback.then(function(data) {
         if(data.data.success)
         {
           $mdToast.show(
              $mdToast.simple()
                  .content('Expense added successfully!')
                  .hideDelay(3000)
          );
          $state.reload();

         }
       });
   };




});
