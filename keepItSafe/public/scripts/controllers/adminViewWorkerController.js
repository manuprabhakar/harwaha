app.controller("adminViewWorkerController",function($scope,$mdToast,localStorageService,$http,$mdSidenav,$log,apiService,$state,$filter,Upload,$window){
  var type =['Leaflet Distribution','Construction Labour','Raj Mistry','Catering Girls','Catering Boys','Welcome Girls','Event Promoters','Bouncers','Masalchi/Kitchen Helpers','Maid/Sweeper','Others',];
  var locality=['Alampur Gonpura','Anandpuri','Anisabad','Ashiana Nagar','Ashiana-Digha Road','Ashok Nagar','Bahadurpur','Bailey Road','Bairiya','Bakerganj','Barh','Begampur','Beldari Chak','Bhogipur','Bihta','Boring Road','Budha Colony','Chajju Bagh','Chhoti Pahari','Chitkohra','Chitragupta Nagar','Chitrakut Nagar','Danapur','Danapur Road','Danapur-Khagaul Road','Dhanaut','Digha','Dulhin Bazar','East Lakshmi Nagar','Exhibition Road','Fraser Road Area','Ganga Vihar Colony','Gardanibagh','Gaurichak','Ghrounda','Gola Road','Golambar','Gopalpur','Haji Ganj','Hajipur','Hanuman Nagar','Haroon Nagar','Indira Nagar','Indrapuri','Jagdeo Path','Jai Prakash Nagar','Jakkanpur','Jalalpur','Janipur','Kadamkuan','Kanhauli','Kankarbagh','Kankarbagh Road','Karanpura','Kautilya Nagar','Keshri Nagar','Khagaul','Khagaul Road','Khajpura','Khusropur','Kidwaipur Postal Colony','Kidwaipuri','Kothwan','Kumhrar','Kurji','Kurthoul','Muhammadpur Korji','Muhammadpur Korji','Nageshwar Colony','Naubatpur','New Azimabad Colony','New Patliputra Colony','North Shastri Nagar','North Sri Krishna Puri','Pahari','Paijawa','Painal','Pareo','Parsa','Patel Nagar','Patliputra Colony','Patliputra Road','Patna - Bakhtiyarpur Road','Patna - Gaya Road','Phulwari Sharif','Punaichak','Punpun','Rajapur','Rajbansi Nagar','Rajeev Nagar','Rajendra Nagar','Ramjaipal Nagar','Ramkrishan Nagar','Rampur','Ranipur','Rukanpura','Rupaspur','Sabzibagh','Sadhnapuri','Sadikpur','Saguna More','Samanpura','Sandalpur','Shastri Nagar','Sheikhpura','Shikarpur','Shivala Par','Shivpuri','Sipara','Sonepur','Sri Krishna Nagar','Sri Krishna Puri','Sultangunj','Taregana','Tripolia','Vijay Nagar','Yarpur'];
  $scope.balance=0;

  	$scope.area=[];
  	$scope.selectedItem;
  	$scope.selectedItem1;
  	$scope.selectedItemChange = function(item) {
  		var item1;
  		if(item==null){
        // console.log(item);
      }
  		else{
  			if(item=="Leaflet Distribution"){
  						item1='leaflet_distribution';
  					}
  			if(item=="Construction Labour"){
  				item1='labour';
  			}
  			if(item=="Raj Mistry"){
  				item1='rajmistry';
  			}
  			if(item=="Catering Girls"){
  				item1='catering';
  			}
  			if(item=="Catering Girls"){
  				item1='catering';
  			}
  			if(item=="Event Promoters"){
  				item1='promoters';
  			}
  			if(item=="Welcome Girls"){
  				item1='welcome';
  			}
  			if(item=="Catering Boys"){
  				item1='catering_boys';
  			}
  			if(item=="Masalchi/Kitchen Helpers"){
  				item1='masalchi';
  			}
  			if(item=="Bouncers"){
  				item1='bouncer';
  			}
  			if(item=="Maid/Sweeper"){
  				item1='maid';
  			}
  			if(item=="Others"){
  				item1='others'
  			}
  $scope.workerUpdate.workarea.push(item1);
  $scope.area.push(item);
  }
  // console.log($scope.workerUpdate.workarea)
  	};
  	$scope.updateCompanyStatus=false;
  $scope.types=function()
  {
  	return type;
  };
  	(function() {
  		var companyCallback = apiService.allWorker();
  		companyCallback.then(function(data) {
  			$scope.workerArray = data.data;
  			// console.log($scope.workerArray);
  		});
  	})();

  //image upload
  $scope.image=null;
  var vm=this;
  	var up = this;
           $scope.submit = function(){ //function to call on form submit
  					 console.log($scope.image.name);

               if ($scope.image){ //check if from is valid
  							//  console.log("yes");
  								$scope.upload($scope.image); //call upload function
               }
           }
           $scope.upload = function (file) {
               Upload.upload({
                   url: '/api/upload', //webAPI exposed to upload the file
                   data:{file:file} //pass file as data, should be user ng-model
               }).then(function (resp) { //upload function returns a promise
                   if(resp.data.error_code === 0){ //validate success
                       $window.alert('Success ' + resp.config.data.file.name + ' uploaded.');
                   } else {
                       $window.alert('an error occured');
                   }
               }, function (resp) { //catch error
                  //  console.log('Error status: ' + resp.status);
                   $window.alert('Error status: ' + resp.status);
               }, function (evt) {
                  //  console.log(evt);
                   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  //  console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                   vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
               });
  				 };

  	$scope.selectedRowCallback = function(rows) {
  		 $scope.updateWorkerStatus=false;

  	};

  	$scope.deleteRowCallback = function(rows){

  					var companyCallback = apiService.deleteWorker(rows);
  					  companyCallback.then(function(data) {
                // console.log(data);
  					  	if(data.data.success)
  							{
  								$mdToast.show(
  		 							 $mdToast.simple()
  		 									 .content('Worker Data Deleted Successfully!')
  		 									 .hideDelay(3000)
  		 					 );
  							}
  					  })
  				 };


  				 $scope.updateRowCallback = function(rows){
  	             if(rows.length>1)
  							  alert("cannot update multiple rows");

  							  else
  								{
  											// console.log(rows);
  											$scope.workerUpdate = $filter('filter')($scope.workerArray, {_id:rows[0]})[0];
  											// console.log($scope.workerUpdate);
  											$scope.updateCompanyStatus = true;
  							   }

  				 			 };

  	 $scope.updateWorker = function() {
  		//  $scope.workerUpdate.aadharImg=$scope.image.name;
  	    var updateCallback = apiService.updateWorker($scope.workerUpdate);
  			   updateCallback.then(function(data) {
  			   	if(data.data.success)
  					{
  						$mdToast.show(
  							 $mdToast.simple()
  									 .content('Worker Updated successfully!')
  									 .hideDelay(3000)
  					 );
  					}
  					else {
  						alert(data.data.message);
  					}
  				});

  		 $scope.updateCompanyStatus = false;

  	 };






  			$scope.toggleLeft = buildToggler('left');
  				 $scope.toggleRight = buildToggler('right');

  				 function buildToggler(componentId) {
  					 return function() {
  						 $mdSidenav(componentId).toggle();
  					 };
  				 }

  				 $scope.localityUpdate=function(item){
  					 if (item!=null)
  					{ $scope.workerUpdate.locality.push(item);}
  					//  console.log($scope.workerUpdate.locality);
  				 }
  $scope.locality1=function(){return locality;}



  				 $scope.logout = function() {
  				   localStorageService.cookie.clearAll();
  				   $http.defaults.headers.common.Authorization = '';
  				   $state.go("createOrder");
  				 };




});
