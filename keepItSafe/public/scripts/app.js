var app = angular.module("app",['ngRoute','ui.router','angular-loading-bar','ui.bootstrap','ngMaterial','ngAnimate','LocalStorageModule','ngMdIcons','ngSanitize','mdDataTable','ngFileSaver','modal-form','ngFileUpload']);

// app.config(function($routeProvider,$locationProvider) {
//
// 	 $routeProvider
//
//  .when("/login",{
// 	 templateURL : "login.html"
// 	 controller : "loginController"
//
//  })
//
//  .when("/logout",{
// 	 template : "ohh shit",
//
//  })
//
//
//  $locationProvider.html5Mode(true);
// });


app.config(function($stateProvider, $urlRouterProvider,$locationProvider) {

    $urlRouterProvider.otherwise('/createOrder');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        .state('base', {
         abstract: true,
         url: '',
         templateUrl: 'views/base.html',
         controller:"loginController"
       })
       .state('base1', {
        abstract: true,
        url: '',
        templateUrl: 'views/base1.html',
        controller:"createOrderFinalController"
      })
        .state('logon', {
            url: '/logon',
            parent:'base',
            templateUrl: '/views/logon.html',
						controller:"loginController"
        })

        .state('createOrder', {
            url: '/createOrder',
            parent:'',
            templateUrl: '/views/createOrder.html',
            controller:"createOrderController"
        })

      .state('tele', {
          url: '/tele',
          parent:'base',
          templateUrl: '/views/tele.html',
          controller:"teleController"
      })



      .state('createOrderFinal', {
          url: '/createOrderFinal',
          parent:'',

          templateUrl: '/views/createOrderFinal.html',
          controller:"createOrderFinalController"
      })
      .state('booked', {
          url: '/bookedHistory',
          parent:'',
          templateUrl: '/views/booked.html',
          controller:"booked"
      })

        .state('login', {
            url: '/login',
            templateUrl: '/views/login.html',
						controller:"loginController"
        })

// admin panel pages and redirects
        // .state('adminBase', {
        //  abstract: true,
        //  url: '',
        //  templateUrl: 'views/adminBase.html',
        //  controller:"adminDashboardController"
        //   })

          .state('viewWorker', {
              url: '/viewWorker',
              parent:'',
              templateUrl: '/views/addCompany.html',
  						controller:"companyController"
          })

        .state('addProduct', {
            url: '/addProduct',
            parent:'adminBase',
            templateUrl: '/views/addProduct.html',
						controller:"productController"
        })


        .state('404', {
            url: '/404',
            templateUrl: '/views/404.html',
        })


        .state('success',{
          url:'/success/:count/:area',
          parent:'',
          templateUrl:'/views/success.html',
          controller:"success"
        })
        .state('signup',{
          url:'/signup',
          parent:'',
          templateUrl:'/views/signup.html',
          controller:"createOrderController"
        })
        .state('failure',{
          url:'/workersnotfound',
          parent:'',
          templateUrl:'/views/failure.html'
        })
        .state('stateNotAvailable',{
          url:'/stateNotAvailable',
          parent:'',
          templateUrl:'/views/stateFailed.html'
        })
				// $locationProvider.html5Mode(true);

        .state('loginHome',{
          url:'/loginHome',
          parent:'base',
          templateUrl:'/views/loginHome.html',
          controller:"loginController"
        })

        .state('bookings',{
          url:'/bookings',
          parent:'',
          templateUrl:'/views/bookingsContractor.html',
          controller:"bookingContractor"
        })
        .state('myBookings',{
          url:'/mybookings',
          parent:'',
          templateUrl:'/views/mybookingsContractor.html',
          controller:"bookingContractor"
        })
        .state('adminBookings',{
          url:'/adminbookings',
          parent:'',
          templateUrl:'/views/adminViewBookings.html',
          controller:"adminViewBookingController"
        })
        .state('contractorList',{
          url:'/contractorList',
          parent:'',
          templateUrl:'/views/adminViewContractor.html',
          controller:"adminViewBookingController"
        })
        .state('adminViewWorker', {
            url: '/adminvieworker',
            parent:'',
            templateUrl: '/views/adminViewWorker.html',
            controller:"adminViewWorkerController"
        })

        .state('addWorker',{
          url:'/addWorker',
          parent:'',
          templateUrl:'/views/addWorker.html',
          controller:"addWorkerController"
        })
        .state('bookingManager', {
            url: '/bookingManager',
            parent:'',
            templateUrl: '/views/bookingmanager.html',
            controller:"bookingContractor"
        })
        .state('createLogin', {
            url: '/createLogin',
            parent:'',
            templateUrl: '/views/createLogin.html',
            controller:"adminCreateLogin"
        })
        // $locationProvider.html5Mode({
        //   enabled: true,
        //   requireBase: false
        // });
});




app.run(function($rootScope, $http, $location,localStorageService){
      // keep user logged in after page refresh
      if (localStorageService.cookie.get('token')&&localStorageService.cookie.get('login')) {
          $http.defaults.headers.common.Authorization = localStorageService.cookie.get('login').token;
      }

      // redirect to login page if not logged in and trying to access a restricted page
      $rootScope.$on('$locationChangeStart', function (event, next, current) {
          var publicPages = ['/login','/404'];
          var restrictedPage = publicPages.indexOf($location.path()) === -1;
          if (restrictedPage&&(!localStorageService.cookie.get('token')||!localStorageService.cookie.get('login'))) {
              $location.path('/createOrder');
          }
      });
  });






app.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('light-blue')
    .primaryPalette('teal')
    .accentPalette('purple');
});

app.config(function($compileProvider) {
    $compileProvider.preAssignBindingsEnabled(true);
  });

  app.config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
        var m = moment(date);
        return m.isValid() ? m.format('DD/MM/YYYY'): '';
      };
  });
