app.controller("booked",function($scope,apiService,$http,$mdToast,$state,$mdSidenav,$filter,localStorageService,$q,$stateParams){
$scope.userid=localStorageService.cookie.get('userid');
$scope.user=localStorageService.cookie.get('name');
(function() {
  var companyCallback = apiService.getOrderById($scope.userid);
  companyCallback.then(function(data) {
    $scope.order = data.data;
    console.log($scope.order);
  });
})();
$scope.toggleLeft = buildToggler('left');
   $scope.toggleRight = buildToggler('right');

   function buildToggler(componentId) {
     return function() {
       $mdSidenav(componentId).toggle();
     };
   }

   $scope.logout = function() {

     localStorageService.cookie.clearAll();
     $http.defaults.headers.common.Authorization = '';
     $state.go("createOrder");
   };
});
