app.service('apiService',function($http,$filter,FileSaver,Blob,localStorageService){


var self = this;

this.show = " ";


	 this.loginUser = function(email,password) {
	return $http({
					method: "post",
					url:"/api/authenticate",
					data: {
						email:email,
						password:password
					},
					headers: { 'Content-Type': 'application/json' }
			}).then(function(data) {
  			return data;
	});
};
this.createUser = function(user) {
return $http({
			 method: "post",
			 url:"/api/createUser",
			 data: {
				 name:user.name,
				 password:user.password,
				 email:user.email,
				 companyname:user.companyname,
				 phone:user.phone,
				 location:user.location,
				 state:user.state,
				 role:user.role,
			 },
			 headers: { 'Content-Type': 'application/json' }
	 }).then(function(data) {
		 return data;
});
};
this.findWorker=function(type,count){

	return $http({
		method:"post",
		url:"/api/findWorker",
		data:{workarea:type,worker_no:count},
		headers: { 'Content-Type': 'application/json' }
	}).then(function(data){
		return data;

	});
};
this.allWorker = function() {
	return $http({
		method:"GET",
		url:"/api/showAllWorker"
	}).then(function(data) {
			return data;
	});
};
//*************************************** PRODUCT API CALL ******************************************************************

this.createProduct = function(name,rate) {

    return $http({
      method:"post",
      url:"/api/createProduct",
      data:{
         name:name,
         rate:rate
      },
      headers:{'Content-Type':'application/json'}
    }).then(function(data){
       return data;
    });

};

this.createWorker=function(worker){
	return $http({
		method:"post",
		url:"/api/createWorker",
		data:{
			 name:worker.name,
			 address:worker.address,
			 phone:worker.phone,
			 smartphone:worker.smartphone,
			 vehicle:worker.vehicle,
			 outstation:worker.outstation,
			 workarea:worker.workarea,
			 isbooked:worker.isbooked,
			 qualification:worker.qualification,
			 aadharimg:worker.aadharImg,
			 locality:worker.locality
		},
		headers:{'Content-Type':'application/json'}
	}).then(function(data){
		 return data;
	});

};
this.updateWorker = function(worker) {

	return $http({
		method:"post",
		url:"/api/updateWorker",
		headers:{"Content-Type":"application/json"},
		data:{
		 _id:worker._id,
		 name:worker.name,address:worker.address,phone:worker.phone,
		 smartphone:worker.smartphone,vehicle:worker.vehicle,qualification:worker.qualification,
	 outstation:worker.outstation,workarea:worker.workarea,locality:worker.locality,isbooked:worker.isbooked,
 aadharimg:worker.aadharimg
	 }
	}).then(function(data) {
			return data;
	});
};
this.getBookingsById = function(id) {

	return $http({
	 method:"post",
	 url:"/api/getBookingsById",
	 headers:{"Content-Type":"application/json"},
	 data:{
		 _id:id
	 }
 }).then(function(data) {
		 return data;
 });


};

  this.allProduct = function() {
    return $http({
      method:"GET",
      url:"/api/showAllProducts"
    }).then(function(data) {
        return data;
    });
 };

 this.deleteProduct = function(rows) {

   return $http({
     method:"post",
     url:"/api/deleteProduct",
     headers:{"Content-Type":"application/json"},
     data:rows

   }).then(function(data) {
       return data;

   });

 };


 this.updateProduct = function(product) {

   return $http({
     method:"post",
     url:"/api/updateProduct",
     headers:{"Content-Type":"application/json"},
     data:{
			 _id:product._id,
			 name:product.name,
			 rate:product.rate,
		 }
   }).then(function(data) {
       return data;
   });
 };
 //***************************************  ALLOTMENT API CALLS ******************************************************************

 this.createAllotmentProduct = function(name,product) {

   console.log(product);
	 return $http({
		 method:"post",
		 url:"/api/createAllotmentProduct",
		 headers:{"Content-Type":"application/json"},
		 data:{
			 product:name.name,
			 requestedQuantity:product.requestedQuantity
		 }
	 }).then(function(data) {
			 return data;
	 });
 };

 this.updateAllotmentProduct = function(allotment) {
 	return $http({
		method:"post",
		url:"/api/updateAllotmentProduct",
		headers:{"Content-Type":"application/json"},
		data:{
			_id:allotment.id,
			supplier:allotment.supplier,
			container:allotment.container,
			rate:allotment.rate,
			quantityDispatched:allotment.quantityDispatched,
			mark:allotment.mark,
			vendor:allotment.vendor,
			recieved:allotment.date,
			product:allotment.product
		}
	}).then(function(data){
		 return data;
	});
};

 this.showAllotments = function() {
	 return $http({
 		method:"GET",
 		url:"/api/showAllotments"
 	}).then(function(data) {
 			return data;
 	});
};

this.createAllotmentOption = function(role) {

	console.log(role);
	return $http({
		method:"post",
		url:"/api/createAllotmentOption",
		headers:{"Content-Type":"application/json"},
		data:{
			roleName:role.type,
			name:role.name
		}
	}).then(function(data) {
			return data;
	});

};

this.getAllotmentOption = function() {

	return $http({
	 method:"GET",
	 url:"/api/getAllotmentOption"
 }).then(function(data) {
		 return data;
 });
};



this.updateAllotmentByID = function(updateLot) {

	return $http({
	 method:"post",
	 url:"/api/updateAllotmentById",
	 headers:{"Content-Type":"application/json"},
	 data:{
		 id:updateLot.id,
		 idA:updateLot.idA,
		 status:updateLot.status
	 }
	}).then(function(data) {
		console.log(data);
		 return data;
	});

};

this.getAllotmentBySupplier = function(supplierData) {

	return $http({
	 method:"post",
	 url:"/api/getAllotmentBySupplier",
	 headers:{"Content-Type":"application/json"},
	 responseType: 'blob',
	 data:{
		 supplier:supplierData.supplier,
		 startAt:$filter('date')(supplierData.startAt, "yyyy-MM-dd", "+0530"),
		 endAt:$filter('date')(supplierData.endAt, "yyyy-MM-dd", "+0530")
	 }
	}).then(function(data) {
	  var blob = new Blob([data.data], { type: 'application/vnd.ms-excel' });
      FileSaver.saveAs(blob, "excelsheet.xlsx");
		 return data;
	});

};

this.getAllotmentByStatus = function(supplierData) {

	return $http({
	 method:"post",
	 url:"/api/getAllotmentByStatus",
	 headers:{"Content-Type":"application/json"},
	 responseType: 'blob',
	 data:{
		 status:"NR",
	 }
	}).then(function(data) {
	  var blob = new Blob([data.data], { type: 'application/vnd.ms-excel' });
      FileSaver.saveAs(blob, "excelsheetProduct.xlsx");
		 return data;

	});

};





 //***************************************  ALLOTMENT API CALLS ******************************************************************

 //***************************************  EXPENSES API CALLS ******************************************************************

 this.createExpense = function(expense) {

    return $http({
			method:"post",
			url:"/api/createExpense",
			data:{
				type:expense.type,
				paidFor:expense.paidFor,
				rate:expense.rate
			},
			headers:{'Content-Type':'application/json'}
		}).then(function(data) {
			return data;
		});


 };


this.getExpenses = function() {
	return $http({
		method:"GET",
		url:"/api/getExpenses"
	}).then(function(data) {
		  return data;
	});
};

this.getExpensesByType = function(expenseData) {
  console.log(expenseData);
	return $http({
	 method:"post",
	 url:"/api/getExpensesByType",
	 headers:{"Content-Type":"application/json"},
	 responseType: 'blob',
	 data:{
		 type:expenseData.type,
		 startAt:$filter('date')(expenseData.startAt, "yyyy-MM-dd", "+0530"),
		 endAt:$filter('date')(expenseData.endAt, "yyyy-MM-dd", "+0530")
	 }
 }).then(function(data) {

	  var blob = new Blob([data.data], { type: 'application/vnd.ms-excel' });
      FileSaver.saveAs(blob, "expenses.xlsx");
		 return data;

	});

};




 //***************************************  EXPENSES API CALLS ******************************************************************



 //*************************************** COMPANY API CALL ******************************************************************

 this.createCompany = function(company) {

     return $http({
       method:"post",
       url:"/api/createCompany",
       data:{
          name:company.name,
          address:company.address,
					phone:company.phone,
					gstin:company.gstin,
					state:company.state,
					stateCode:company.stateCode
       },
       headers:{'Content-Type':'application/json'}
     }).then(function(data){
        return data;
     });

 };

   this.allCompany = function() {
     return $http({
       method:"GET",
       url:"/api/showAllCompany"
     }).then(function(data) {
         return data;
     });
  };
	this.showAllBookings = function() {
		return $http({
			method:"GET",
			url:"/api/showAllBookings"
		}).then(function(data) {
				return data;
		});
	};
	this.adminBookings = function() {
		return $http({
			method:"GET",
			url:"/api/adminShowBookings"
		}).then(function(data) {
				return data;
		});
	};
	this.adminContractor = function() {
		return $http({
			method:"GET",
			url:"/api/adminShowContractor"
		}).then(function(data) {
				return data;
		});
	};
  this.deleteWorker = function(rows) {

    return $http({
      method:"post",
      url:"/api/deleteWorker",
      headers:{"Content-Type":"application/json"},
      data:rows

    }).then(function(data) {
        return data;

    });

  };

this.updateBalance = function(order) {
	return $http({
		method:"post",
		url:"/api/updateBalance",
		data:{
			balance:order.totalPrice,
			_id:order.company_id
		},
		headers:{"Content-Type":"application/json"}
	}).then(function(data){

		  return data;

	});

};

this.getCompanyById = function(Id) {
   console.log(Id);
	return $http({
	 method:"post",
	 url:"/api/getCompanyById",
	 data:{
		 id:Id
	 },
	 headers:{"Content-Type":"application/json"}
 }).then(function(data){

		 return data;

 });


};



this.updateBalanceDec = function(order,paymentStatus) {
var temp = order.totalPrice;
if(paymentStatus!="recieved"&&paymentStatus!="returned"&&paymentStatus!="cancelled")
  {temp = 0}


	return $http({
		method:"post",
		url:"/api/updateBalanceDec",
		data:{
			balance:temp,
			_id:order.company_id
		},
		headers:{"Content-Type":"application/json"}
	}).then(function(data){

		  return data;

	});

};

this.updateBalanceDecTele = function(update) {


	return $http({
		method:"post",
		url:"/api/updateBalanceDec",
		data:{
			balance:update.totalPrice,
			_id:update.company_id
		},
		headers:{"Content-Type":"application/json"}
	}).then(function(data){

		  return data;

	});

};







  this.updateCompany = function(company) {

    return $http({
      method:"post",
      url:"/api/updateCompany",
      headers:{"Content-Type":"application/json"},
      data:{
 			 _id:company._id,
       name:company.name,
			 address:company.address,
 			 phone:company.phone
 		 }
    }).then(function(data) {
        return data;
    });
  };





//*************************************** ORDER API CALL ******************************************************************

   this.getOrderByCompanyId = function(id) {

		   return $http({
				 method:"POST",
				 url:"/api/getOrderByCompanyId",
				 data:{
					 companyId:id
				 },
				 headers:{"Content-Type":"application/json"}
			 }).then(function(data) {
			 	return data;
			});

   };

this.createOrder = function(order) {
   return $http({
		 method:"post",
		 url:"/api/createOrder",
		 data:{
			 userid:order.userid,
			 totalPrice:order.totalPrice,
			 worktype:order.worktype,
			 product:order.product,
			 numberofworkers:order.numberOfWorkers,
			 createdAt:order.createdAt,
			 user:localStorageService.cookie.get('login').user,
			 username:order.name,
			 phone:order.phone,
			 email:order.email,
			 deliveryDate:order.deliveryDate

		 },
		 headers:{"Content-Type":"application/json"}
	 }).then(function(data){
		   return data;
	 });

};


this.getUserOrder = function() {
	return $http({
		method:"post",
		url:"/api/getUserOrder",
		data:{
			user:localStorageService.cookie.get("login").user
		},
		headers:{"Content-Type":"application/json"}
	}).then(function(data) {
		  return data;
	});

};


this.getAllOrders = function() {
  return $http({
		method:"GET",
		url:"/api/getAllOrders"
	}).then(function(data) {
		  return data;
	});

};

this.getOrderByQuery = function(order){
 	return $http({
 		method:"post",
 		url:"/api/getOrderByQuery",
 		data:{
 			status:order.status,
 		  paymentStatus:order.paymentStatus,
 			startAt:$filter('date')(order.startAt, "yyyy-MM-dd", "+0530"),
 			endAt:$filter('date')(order.endAt, "yyyy-MM-dd", "+0530")
 		},
 		headers:{"Content-Type":"application/json"}
 	}).then(function(data) {
 		  return data;
 	});
 };

 this.getOrderById = function(id) {
	 return $http({
		 method:"post",
		 url:"/api/getOrderById",
		 data:{
			 id:id,
		 },
		 headers:{"Content-Type":"application/json"}
	 }).then(function(data) {
			 return data;
	 });
 };

this.updateOrder = function(update) {

	  return $http({
			method:"post",
			url:"/api/updateOrder",
			data:{
				id:update.id,
				cId:update.cId,
				cname:update.cname
			},
			headers:{"Content-Type":"application/json"}

		}).then(function(data) {
			return data;
		});
};


this.updateOrderTele = function(update) {
	console.log(update);
	  return $http({
			method:"post",
			url:"/api/updateOrderTele",
			data:{
				id:update.id,
        status:update.status,
				dateModified:$filter('date')(new Date(), "yyyy-MM-dd", "+0530")
			},
			headers:{"Content-Type":"application/json"}

		}).then(function(data) {
			return data;
		});
};

this.assignUpdate = function(assign){
	return $http({
		method:"post",
		url:"/api/assignUpdate",
		data:{
			name:assign.name,
			id:assign.id,
			status:"dispatched"
		},
		headers:{"Content-Type":"application/json"}
	}).then(function(data) {
		return data;
	});

};

this.getOrderReport = function(expenseData) {
  console.log(expenseData);
	return $http({
	 method:"post",
	 url:"/api/getOrderReport",
	 headers:{"Content-Type":"application/json"},
	 responseType: 'blob',
	 data:{
		 user:expenseData.user,
		 startAt:$filter('date')(expenseData.startAt, "yyyy-MM-dd", "+0530"),
		 endAt:$filter('date')(expenseData.endAt, "yyyy-MM-dd", "+0530")
	 }
 }).then(function(data) {

	  var blob = new Blob([data.data], { type: 'application/vnd.ms-excel' });
      FileSaver.saveAs(blob, "expenses.xlsx");
		 return data;

	});

};
//*************************************** ORDER API CALL ******************************************************************



this.killTheDb = function() {
		return $http({
			method:"GET",
			url:"/api/killTheDb",
		}).then(function(data) {
		 return data;
	 });

};


this.convertNumberToWords=function(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores\n ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs\n";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
};

this.verify=function(phone){
return	$http({
						method: "post",
						url:"/api/verify",
						data: {
							number:phone
						},
						headers: { 'Content-Type': 'application/json' }
				}).then(function(data) {
				return data;

		});
}
this.otpverify=function(otp){
return	$http({
						method: "post",
						url:"/api/otpVerify",
						data: {
							pass:otp.password,
							sessionId:otp.sessionId
						},
						headers: { 'Content-Type': 'application/json' }
				}).then(function(data) {
				return data;

		});
}



});
