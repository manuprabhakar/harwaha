app.controller("paymentsController",function($scope,$http,apiService,$filter,$state,localStorageService){

$scope.orderArray = [];

  (function() {
    var callback = apiService.getAllOrders();
    callback.then(function(data){
      $scope.orderArray = data.data;
      for(i in $scope.orderArray){
        $scope.orderArray[i].createdAt = $filter("date")($scope.orderArray[i].createdAt,'dd/MM/yyyy');
      }
    });
  }

)();

});
