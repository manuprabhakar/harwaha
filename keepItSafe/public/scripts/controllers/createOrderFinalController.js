app.controller("createOrderFinalController",function($scope,apiService,$http,$mdToast,$state,$mdSidenav,$timeout,$filter,localStorageService,$q,$stateParams){
$scope.user=localStorageService.cookie.get('name');
$scope.userid=localStorageService.cookie.get('userid');
$scope.phone=localStorageService.cookie.get('phone');
$scope.email=localStorageService.cookie.get('email');
console.log($scope.userid);
console.log($scope.user);
console.log($scope.email);
$scope.area;
$scope.count="";
$scope.workarea="";
$scope.worker_no="";
$scope.states=["Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Dadra and Nagar Haveli","Daman and Diu","Delhi","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka",
                                        "Kerala","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Puducherry","Punjab", "Rajasthan","Sikkim","Tamil Nadu",
                                        "Telangana","Tripura","Uttar Pradesh","Uttarakhand","West Bengal"];
(function(){
  this.deliveryDate1 = new Date();
  this.isOpen = false;
})();
$scope.setlocality=function(){
if($scope.area=="Bihar"){
  $scope.locality=['Patna'];
}else{
$scope.locality=[''];
}
}
$scope.workerFind=function(){
  $scope.deliveryDate=new Date(this.deliveryDate1);
  $scope.order={userid:$scope.userid,name:$scope.user,email:$scope.email,phone:$scope.phone,worktype:$scope.workarea,numberOfWorkers:$scope.worker_no,totalPrice:'',deliveryDate:$scope.deliveryDate};

  localStorageService.cookie.set('order',$scope.order);
  console.log($scope.order);
  console.log($scope.deliveryDate1);
  if($scope.area=="Bihar"){
  var callback=apiService.findWorker($scope.workarea,$scope.worker_no);
  callback.then(function(data){
    if(data.data.isCount)
    {
        var worktype=$scope.workarea;
        $state.go('success',{'count':$scope.worker_no, 'area':worktype});
      }
        else {
          $state.go('failure');
        }

  });
}
  else{
    $state.go('stateNotAvailable');}
  };
$scope.logout = function() {
  localStorageService.cookie.clearAll();
  $http.defaults.headers.common.Authorization = '';
  $state.go("createOrder");
};
  $scope.toggleLeft = buildToggler('left');
     $scope.toggleRight = buildToggler('right');

     function buildToggler(componentId) {
       return function() {
         $mdSidenav(componentId).toggle();
       };
     }

});
