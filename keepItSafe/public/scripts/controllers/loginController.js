app.controller("loginController",function($scope,$http,$mdDialog,localStorageService,apiService,$state){


 $scope.name = "hello";
 var user = {
   user:'',password:''
 };




 $scope.createUser = function() {
 console.log("hello");
 $http({
          method: "post",
          url:"/api/createUser",
          data: {
            user:$scope.test.user,
            password:$scope.test.password,
            role:$scope.test.role
          },
          headers: { 'Content-Type': 'application/json' }
      }).then(function(data) {
      console.log(data);
      alert("form submitted")
  });

};

$scope.loginUser = function() {
       var dataCallback = apiService.loginUser($scope.user.user,$scope.user.password);

         dataCallback.then(function(data){

             $scope.data = data.data;
             if($scope.data.success)
             {
               $scope.showAlert("Login Successfully","Hi,"+data.data.user+" have a great day!!!");
               var login = {
                 token:data.data.token,
                 user:data.data.user,
                 role:$scope.data.role
               }
               localStorageService.cookie.set('login',login);
               localStorageService.cookie.set('token',login.token);
               $http.defaults.headers.common.Authorization =login.token;


               if($scope.data.role=="admin")
               {
                 $state.go('adminDashboard');
               }

               else if($scope.data.role=="tele")
               {
                 $state.go('createOrder');
               }

               else if($scope.data.role=="sales")
               {
                 $state.go('salesDashboard');
               }

               else if($scope.data.role=="workshop")
               {
                 $state.go('workshop');
               }
             }
             else
             alert("wrong user");

         });
};

$scope.logout = function() {
  localStorageService.cookie.clearAll();
  $http.defaults.headers.common.Authorization = '';
  $state.go("login");
}


$scope.showAlert = function(read,message) {
    // Appending dialog to document.body to cover sidenav in docs app
    // Modal dialogs should fully cover application
    // to prevent interaction outside of dialog
    $mdDialog.show(
      $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title(read)
        .textContent(message)
        .ariaLabel('login Dialog ')
        .ok('Got it!')

    );
  };
});
