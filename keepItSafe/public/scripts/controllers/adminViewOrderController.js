app.controller("adminViewOrderController",function($scope,$http,apiService,$rootScope,$q,$filter,$state,localStorageService){

$scope.orderArray = [];

$scope.startAt = null;
$scope.endAt = null;


$scope.orderFilter = {
  status:'(.*)',
  paymentStatus:'(.*)',
  startAt:null,
  endAt:null
};




  (function() {
    var callback = apiService.getAllOrders();
    callback.then(function(data){
      console.log(data);
      $scope.adminorderArray = data.data;
      console.log($scope.orderArray);
      for(i in $scope.orderArray){
        $scope.adminorderArray[i].createdAt = $filter("date")($scope.orderArray[i].createdAt,'dd/MM/yyyy');
        $scope.adminorderArray[i].dateModified = $filter("date")($scope.orderArray[i].dateModified,'dd/MM/yyyy');
        $scope.adminorderArray[i].paymentRecievedAt = $filter("date")($scope.orderArray[i].paymentRecievedAt,'dd/MM/yyyy');
        $scope.adminorderArray[i].totalPrice=$filter('currency')($scope.orderArray[i].totalPrice,'₹', 2)

      }
    });
  }
)();




$scope.selecteddateStart=function(){
 $scope.orderFilter.startAt = $scope.startAt;
}

$scope.selecteddateEnd=function(){
 $scope.orderFilter.endAt = $scope.endAt;
};








$scope.searchFilter = function() {
  var callback = apiService.getOrderByQuery($scope.orderFilter);

   callback.then(function(data) {

     $scope.orderArray = data.data;
     console.log($scope.orderArray);
     for(i in $scope.orderArray){
       $scope.orderArray[i].createdAt = $filter("date")($scope.orderArray[i].createdAt,'dd/MM/yyyy');
       $scope.orderArray[i].dateModified = $filter("date")($scope.orderArray[i].dateModified,'dd/MM/yyyy');
       $scope.orderArray[i].paymentRecievedAt = $filter("date")($scope.orderArray[i].paymentRecievedAt,'dd/MM/yyyy');
       $scope.orderArray[i].totalPrice=$filter('currency')($scope.orderArray[i].totalPrice,'₹', 2)

     }
   });

};

$scope.editOrder = function(item) {
  localStorageService.cookie.set("id",item.rowId,2);
  console.log(item.rowId);
  $state.go("adminEditOrder");
};


//for order update views

$scope.updateDate = new Date();

$scope.selecteddateUpdate=function(){
 $scope.updateArray.updateDate = $scope.updateDate;
 console.log($scope.updateArray.updateDate);
};

$scope.checkAndDecide =function(){
  if($scope.order.paymentStatus!=$scope.updateArray.paymentStatus)
     $scope.updateDate = new Date();

  else{
    $scope.updateDate = $scope.tempDate;
  }

 console.log("hello");

};
$scope.checkAndDecide2 =function(){
  if($scope.order.status!=$scope.updateArray.status)
     $scope.dateModified = new Date();

  else{
    $scope.dateModified = $scope.tempDate2;
  }
};

(function(){
 var id = localStorageService.cookie.get("id");
  var callback = apiService.getOrderById(id);
  callback.then(function(data) {
   $scope.tempDate = new Date(data.data[0].paymentRecievedAt);
   $scope.tempDate2 = new Date(data.data[0].dateModified);

   $scope.dateModified = new Date(data.data[0].dateModified);

   $scope.updateDate = new Date(data.data[0].paymentRecievedAt);

    for(i in data.data){
      data.data[i].createdAt = $filter("date")(data.data[i].createdAt,'dd/MM/yyyy');
      data.data[i].dateModified = $filter("date")(data.data[i].dateModified,'dd/MM/yyyy');
      data.data[i].paymentRecievedAt = $filter("date")(data.data[i].paymentRecievedAt,'dd/MM/yyyy');

    }
    $scope.order=data.data[0];
    console.log($scope.order);


   console.log( $scope.updateDate);

    $scope.updateArray = {
      paymentStatus:$scope.order.paymentStatus,
      orderType:$scope.order.orderType,
      paymentType:$scope.order.paymentType,
      id:localStorageService.cookie.get("id"),
      updateDate:$scope.updateDate,
      status:$scope.order.status,
      dateModified:$scope.dateModified
    };

  });
 })();

 $scope.updateOrder = function() {
  if(($scope.order.paymentStatus==$scope.updateArray.paymentStatus))
    {$scope.order.totalPrice = 0;}

    if(($scope.order.paymentStatus=='recieved'&&$scope.updateArray.paymentStatus=='returned'))
      {$scope.order.totalPrice = 0;}

   var request = [apiService.updateOrder($scope.updateArray),apiService.updateBalanceDec($scope.order,$scope.updateArray.paymentStatus)];


    $q.all(request).then(function(data){
       if(data[0].data.success&&data[1].data.success)
       {
           // var callback = apiService.createPayment()

           alert("Order updated Successfully!!");
           $state.go("adminViewOrder");
       }
       else {
         alert("oops something went wrong! Please try again!!")
       }

    });


 };





// $scope.updateOrder = function() {
//  var callback = apiService.updateOrder($scope.updateArray);
//
//  callback.then(function(data) {
//     if (data.data.success) {
//       alert("Order updated");
//
//     }
//    else {
//      alert("oops something went wrong!!");
//    }
//
//  });
// };

$scope.updateOrderPaymentStatus = function() {
 var callback = apiService.updateOrderPaymentStatus($scope.updateArray);

 callback.then(function(data) {

    if (data.data.success) {
      alert("status updated");
    }

   else {
     alert("oops something went wrong!!");
   }

 });
};


});





////was a test
// app.filter('daterange', function ()
// {
//   return function(conversations, start_date, end_date)
// 	{
// 		var result = [];
//
// 		// date filters
// 		var start_date = (start_date && !isNaN(Date.parse(start_date))) ? Date.parse(start_date) : 0;
// 		var end_date = (end_date && !isNaN(Date.parse(end_date))) ? Date.parse(end_date) : new Date().getTime();
//
// 		// if the conversations are loaded
// 		if (conversations && conversations.length > 0)
// 		{
// 			$.each(conversations, function (index, conversation)
// 			{
// 				var conversationDate = new Date(conversation.date_posted);
//
// 				if (conversationDate >= start_date && conversationDate <= end_date)
// 				{
// 					result.push(conversation);
// 				}
// 			});
//
// 			return result;
// 		}
// 	};
// });
